var VRControls = (function ()
{
  // init objects
  var scene;
  var camera;
  var renderer;
  var element;

  var scriptsLoaded = 0;
  var libraryUrls = ["js/OrbitControls.js", "js/DeviceOrientationControls.js", "js/StereoEffect.js"];

  var clock = new THREE.Clock();
  var effect;
  var controls;

  // load external scripts
  var loadScripts = function( urls, callback )
  {
    for(var x = 0; x < urls.length; x++)
    {
      // make script element
      var script = document.createElement( "script" )
      script.type = "text/javascript";

      // load script
      if(script.readyState)
      {
        //IE
        script.onreadystatechange = function()
        {
          if ( script.readyState === "loaded" || script.readyState === "complete" )
          {
            script.onreadystatechange = null;
            callback();
          }
        };
      }
      else
      {
        //Others
        script.onload = function()
        {
          callback();
        };
      }

      // attach script to script element and append to html document
      script.src = urls[x];
      document.getElementsByTagName( "head" )[0].appendChild( script );

      // script loaded
      console.log("getting script: " + urls[x]);
    }
  }

  // load libraries
  loadScripts(libraryUrls, function()
  {
    console.log('loading...');
    scriptsLoaded++;
    if(scriptsLoaded == libraryUrls.length)
    {
      VRControls.load();
    }
  });

  // check if user uses a mobile device
  var isMobileDevice = function ()
  {
      var isMobile = false; //initiate as false

      // device detection
      if(/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|ipad|iris|kindle|Android|Silk|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(navigator.userAgent)
      || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(navigator.userAgent.substr(0,4))) isMobile = true;

      return isMobile;
  };

  // confirm if mobile device is being used
  console.log("mobile device: " + isMobileDevice());


  // create orientation controls
  var initControls = function (_scene, _camera, _renderer, _element)
  {
    scene = _scene;
    camera = _camera;
    renderer = _renderer;
    element = _element;

    // if a mobile device is detected render vr display and controls
    if(isMobileDevice())
    {
        // make stereoscopic effect for VR
        effect = new THREE.StereoEffect(renderer);
    }

    // create orbit controls if orientation controls are not available
    controls = new THREE.OrbitControls(camera, element);
    controls.target.set(
      camera.position.x + 0.15,
      camera.position.y,
      camera.position.z
    );
    controls.noPan = true;
    controls.noZoom = true;

    // Preferred controls via DeviceOrientation
    function setOrientationControls(e) {
      if (!e.alpha) {
        return;
      }

      controls = new THREE.DeviceOrientationControls(camera, true);
      controls.connect();
      controls.update();

      element.addEventListener('click', fullscreen, false);

      window.removeEventListener('deviceorientation', setOrientationControls, true);
    }
    window.addEventListener('deviceorientation', setOrientationControls, true);
  }

 var renderControls = function (render)
 {
    // when not a mobile device disable vr stereo display
    if(!isMobileDevice())
    {
        // render the scene
        renderer.render(scene, camera);
    }
    setTimeout( function() {

        // render again maintaining 60fps (loop)
        requestAnimationFrame(render);

        // when using a mobile device start rendering stereo vr display
        if(isMobileDevice())
        {
            // update movement of camera
            update(clock.getDelta());
            renderEffect(clock.getDelta());
        }

    }, 1000 / 60 );
  }

  // resize screen
  var resize = function ()
  {
      var width = window.innerWidth;
      var height = window.innerHeight;

      camera.aspect = width / height;
      camera.updateProjectionMatrix();

      renderer.setSize(width, height);
      effect.setSize(width, height);
  }

  // update camera projection
  var update = function (dt)
  {
      resize();

      camera.updateProjectionMatrix();

      controls.update(dt);
  }

  // render camera pov
  var renderEffect = function (dt)
  {
      effect.render(scene, camera);
  }

  // make browser fullscreen on mobile device
  var fullscreen = function ()
  {
      if (document.body.requestFullscreen) {
        document.body.requestFullscreen();
      } else if (document.body.msRequestFullscreen) {
        document.body.msRequestFullscreen();
      } else if (document.body.mozRequestFullScreen) {
        document.body.mozRequestFullScreen();
      } else if (document.body.webkitRequestFullscreen) {
        document.body.webkitRequestFullscreen();
      }
  }

  return {
    //check if user device is a mobile device
    isMobileDevice: isMobileDevice,
    //initiate controls for the device
    initControls: initControls,
    //render scene with controls
    renderControls: renderControls,
    //load scripts
    load: function(){},
  };
})();
