VRControls.load = function()
{
  VRControls.isMobileDevice();
  // Change message for mobile vr users
  if(VRControls.isMobileDevice() == true)
  {
      $('#loading').html('Please wait<br>Calculating song tempo and preparing the experience<br>Please put your device in the virtual reality headset');
  }

  // check if webaudiokit is available
  window.AudioContext = window.AudioContext || window.webkitAudioContext || window.mozAudioContext;

  // check if blob is available
  var blob = window.URL || window.webkitURL;
  if (!blob) {
      console.log('Your browser does not support Blob URLs');
  }

  // start calculating tempo in music
  var context = new AudioContext();
  var fileInput = document.getElementById("fileInput");
  var files = [];
  var tempo;
  var bpm;

  // when user has selected an audio file start calculating
  fileInput.onchange = function ()
  {
      $('#fileInput').animate({opacity: 0}, 1000);
      $('#loading').animate({opacity: 1}, 1000);
      files = fileInput.files;

      // if there are no files return
      if (files.length == 0) return;
      var reader = new FileReader();

      // when there is a file start reading and decoding the file
      reader.onload = function(fileEvent)
      {
          context.decodeAudioData(fileEvent.target.result, calcTempo);
      }

      reader.readAsArrayBuffer(files[0]);
  }

  // calculate the tempo of the song
  var calcTempo = function (buffer) {
      var audioData = [];
      // Take the average of the two channels
      if (buffer.numberOfChannels == 2)
      {
          var channel1Data = buffer.getChannelData(0);
          var channel2Data = buffer.getChannelData(1);
          var length = channel1Data.length;

          for (var i = 0; i < length; i++)
          {
            audioData[i] = (channel1Data[i] + channel2Data[i]) / 2;
          }
      }
      else
      {
          audioData = buffer.getChannelData(0);
      }

      // music tempo data (BPM)
      try{
        var mt = new MusicTempo(audioData);
      } catch (error) {
          console.error(error);
          
      }

      console.log(mt.tempo);

      tempo = mt.tempo / 100;
      bpm = mt.tempo;

      // when music tempo has been succesfully calculated. place the song into the audio player.
      var file = files[0],
      fileURL = blob.createObjectURL(file);
      console.log(file);
      console.log('File name: '+file.name);
      console.log('File type: '+file.type);
      console.log('File BlobURL: '+ fileURL);
      document.getElementById('audio').src = fileURL;

      // after calculating the tempo and placing the song into the audio player the animation can start
      $('#loading').css({left: '45%'});
      if(VRControls.isMobileDevice() == true)
      {
          $('#loading').html('Touch the screen to start the audio');
      }
      else
      {
          $('#loading').html('Get ready');
      }

      $('#loading').animate({opacity: 0}, 2500, startAnimation);
  }

  // start threejs animation
  function startAnimation()
  {
      // get the audio element
      var audio = document.getElementById('audio');

      // init audio analyser and data
      var ctx = new AudioContext();
      var analyser = ctx.createAnalyser();
      var audioSrc = ctx.createMediaElementSource(audio);
      // we have to connect the MediaElementSource with the analyser
      audioSrc.connect(analyser);
      analyser.connect(ctx.destination);
      // we could configure the analyser: e.g. analyser.fftSize (for further infos read the spec)
      analyser.fftSize = 256;
      // frequencyBinCount tells you how many values you'll receive from the analyser
      var frequencyData = new Uint8Array(analyser.frequencyBinCount);

      // number of frequencies to calculate
      var meterNum = 35;

      // make scene for threejs
      var scene = new THREE.Scene();

      // make camera for scene and position it
      var camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);

      // position camera and add to scene
      camera.position.set(0, 0, 50);
      scene.add(camera);

      // create renderer
      var renderer = new THREE.WebGLRenderer();
      var element = renderer.domElement;

      // append renderer
      document.body.appendChild(element);

      // set rendersize to fullscreen
      renderer.setSize(window.innerWidth, window.innerHeight);

      VRControls.initControls(scene, camera, renderer, element);

      // init geometry mesh and material
      var bar = new THREE.BoxGeometry(5, 60, 5);
      var lungs = new THREE.BoxGeometry(30, 10, 2000);
      var heart = new THREE.BoxGeometry(30, 10, 2000);

  //            dub/reggae: 60-90 bpm
  //            downtempo/chillout: 90-120 bpm
  //            deep house: 120-125 bpm
  //            house: 120-130 bpm
  //            tech house: 120-130 bpm
  //            electro house: 125-130 bpm
  //            progressive house: 125-130 bpm
  //            trance: 130-135 bpm
  //            dubstep: 130-145 bpm
  //            techno: 130-150 bpm
  //            hard house: 145-150 bpm
  //            jungle: 155-180 bpm
  //            drum and bass: 165-185 bpm
  //            hardcore/gabber: 160-200 bpm

      var colorBar;
      var colorHeart;
      var colorLungs;

      // reggae
      if(bpm < 90)
      {
          colorBar = 0x27ae60;
          colorHeart = 0x1abc9c;
          colorLungs = 0x2ecc71;
      }
      // chill
      else if(bpm > 90 && bpm < 120)
      {
          colorBar = 0xecf0f1;
          colorHeart = 0xbdc3c7;
          colorLungs = 0x2c3e50;
      }
      // house
      else if(bpm > 120 && bpm < 130)
      {
          colorBar = 0x16a085;
          colorHeart = 0xe74c3c;
          colorLungs = 0x9b59b6;
      }
      // trance
      else if(bpm > 130 && bpm < 150)
      {
          colorBar = 0x3498db;
          colorHeart = 0x9b59b6;
          colorLungs = 0x2980b9;
      }
      // drum and bass/electro
      else if(bpm > 150 && bpm < 180)
      {
          colorBar = 0xf1c40f;
          colorHeart = 0xf39c12;
          colorLungs = 0xe67e22;
      }
      // hardcore
      else if(bpm > 180)
      {
          colorBar = 0x34495e;
          colorHeart = 0x7f8c8d;
          colorLungs = 0x2c3e50;
      }

      var materialBar = new THREE.MeshBasicMaterial({ color: colorBar });
      var materialLungs = new THREE.MeshBasicMaterial({ color: colorHeart });
      var materialHeart = new THREE.MeshBasicMaterial({ color: colorLungs });

      // random degrees for cristal like spawning of equalizer
      var degrees = [10, -10, 0];

      // arrays for equalizer
      var barsLeft = [];
      var barsRight = [];
      var frequencies = [];

      // create equalizer
      for (var i = 0; i <= 35; ++i) {
          barsLeft[i] = new THREE.Mesh(bar, materialBar);

          barsLeft[i].position.z = -i*20;
          barsLeft[i].position.x = -50;

          barsRight[i] = new THREE.Mesh(bar, materialBar);

          barsRight[i].position.z = -i*20;
          barsRight[i].position.x = 50;

          scene.add(barsLeft[i], barsRight[i]);
      }

      // create heart and lungs
      LungLeft = new THREE.Mesh(lungs, materialLungs);
      LungLeft.position.x = -40;
      LungLeft.position.y = -40;
      LungLeft.position.z = -50;
      LungLeft.scale.x = 1;

      LungRight = new THREE.Mesh(lungs, materialLungs);
      LungRight.position.x = 40;
      LungRight.position.y = -40;
      LungRight.position.z = -50;
      LungRight.scale.x = 1;

      Heart = new THREE.Mesh(heart, materialHeart);
      Heart.position.x = 0;
      Heart.position.y = -40;
      Heart.position.z = -50;
      Heart.scale.x = 0.1;

      scene.add(LungLeft, LungRight, Heart);


      // Depending on your age and level of physical fitness, a normal resting pulse ranges from 60 to 80 beats per minute. Your breathing rate is measured in a similar manner, with an average resting rate of 12 to 20 breaths per minute. Both your pulse and breathing rate increase with exercise, maintaining a ratio of approximately 1 breath for every 4 heartbeats.

      //var msPerBeat = 1000 / (tempo / 60);
      var BPS = Math.round(bpm / 60);
      //var sPerBeat = 60 / tempo;
      var secs = 0;
      var expanded = false;
      var beats = 0;

      // loop render frame
      function animate() {

          for(var i = 0; i < barsLeft.length; i++)
          {
              // if bars are out of screen reposition
              if(barsLeft[i].position.z <= 300)
              {
                  barsLeft[i].position.z += tempo;
              }
              else
              {
                  // make bars spawn crystal like
                  var rand = Math.floor((Math.random() * 3) + 0);
//                  barsLeft[i].rotation.z = degrees[rand];
                  barsLeft[i].position.z = -300;
              }
              // apply frequency data of music to bar
              scaleY(barsLeft[i], frequencies[i]);
          }

          // same as above
          for(var i = 0; i < barsRight.length; i++)
          {
              if(barsRight[i].position.z <= 300)
              {
                  barsRight[i].position.z += tempo;
              }
              else
              {
                  var rand = Math.floor((Math.random() * 3) + 0);
//                  barsRight[i].rotation.z = degrees[rand];
                  barsRight[i].position.z = -300;
              }

              scaleY(barsRight[i], frequencies[i]);
          }

          // make the heart pump
          if(secs >= 60)
          {
              secs = 0;
          }

          secs++;

          // (prototype) raw conversion of BPM to heartbeats per second
          for(var i = 1; i < BPS+1; i++)
          {
              if(secs == (60/BPS) * i)
              {
                  if(expanded == false)
                  {
                      Heart.scale.x = 1;
                      expanded = true;

                      // make the lungs breath
                      if(beats > 3)
                      {
                          LungLeft.scale.x = 1;
                          LungRight.scale.x = 1;
                          beats = 0;
                      }

                      else
                      {
                          LungLeft.scale.x -= 0.3;
                          LungRight.scale.x -= 0.3;
                      }

                      beats++;
                  }
                  else
                  {
                      Heart.scale.x = 0.1;
                      expanded = false;
                  }
              }
          }

          VRControls.renderControls(animate);

      };

      // start first render
      animate();

      // loop music analyser
      function renderAudio()
      {
          // init music analyser data
          var array = new Uint8Array(analyser.frequencyBinCount);
          analyser.getByteFrequencyData(array);
          var step = Math.round(array.length / meterNum); //sample limited data from the total array

          // apply all analysed data into array
          for (var i = 0; i < meterNum; i++)
          {
              var value = ((array[i * step])/2.55)/100;

              frequencies[i] = value;
          }

          // analyse audio again (loop)
          requestAnimationFrame(renderAudio);
      }

      // start first audio analyse
      renderAudio();

      // start audio for analyser to analyse (for pc)
      audio.play();

      // scale the mesh shapes and split in half to give illusion of having the bars coming from the ground up
      function scaleY ( mesh, scale ) {
          mesh.scale.y = scale ;
          if( ! mesh.geometry.boundingBox ) mesh.geometry.computeBoundingBox();
          var height = mesh.geometry.boundingBox.max.y - mesh.geometry.boundingBox.min.y;

          mesh.position.y = height * scale / 2 - 20;
      }
  }
};
